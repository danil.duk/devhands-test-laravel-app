<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use Illuminate\Database\DatabaseManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;

final class ApiController extends Controller
{
    public function test(Request $request): JsonResponse
    {
        $blank = $request->boolean('blank');
        if ($blank) {
            return new JsonResponse(['success' => true]);
        }

        $error = $request->integer('error');
        if ($error !== 0 && $error !== 200) {
            return new JsonResponse(
                [
                    'success' => false,
                    'error'   => $error,
                ],
                $error
            );
        }

        return new JsonResponse(
            [
                'success' => true,
                'redis'   => $this->testRedis(
                    $request->integer('redis_query_count'),
                    $request->boolean('redis_multi', false)
                ),
                'pgsql'   => $this->testPgsql(
                    $request->integer('pgsql_query_count'),
                    $request->boolean('pgsql_multi', false)
                ),
                'cpu'     => $this->testCpu(
                    $request->integer('ucpu'),
                    $request->integer('usleep')
                ),
            ]
        );
    }

    private function testRedis(int $queryCount, bool $multi): ?array
    {
        /** @var RedisManager $redisManager */
        $redisManager = app('redis');
        $res          = [];
        $i            = 0;
        if ($multi) {
            if ($queryCount > 0) {
                $keys = [];
                while ($i < $queryCount) {
                    $i++;
                    $keys[] = "test_$i";
                }

                $res = $redisManager->connection('default')->client()->mget($keys);
            }
        } else {
            while ($i < $queryCount) {
                $i++;
                $res[] = $redisManager->connection('default')->client()->get("test_$i");
            }
        }

        return count($res) > 0 ? $res : null;
    }

    private function testPgsql(int $queryCount, bool $multi): ?array
    {
        /** @var DatabaseManager $dbManager */
        $dbManager = app('db');
        $res       = [];
        $i         = 0;
        if ($multi) {
            if ($queryCount > 0) {
                $placeholder = implode(',', array_fill(0, $queryCount, '?'));
                $ids         = [];
                while ($i < $queryCount) {
                    $i++;
                    $ids[] = "$i";
                }

                $res = $dbManager->connection('pgsql')->select("SELECT name, email FROM users where id in ({$placeholder})", $ids);
            }
        } else {
            while ($i < $queryCount) {
                $i++;
                $res[] = $dbManager->connection('pgsql')->selectOne('SELECT name, email FROM users where id = ?', [$i]);
            }
        }

        return count($res) > 0 ? $res : null;
    }

    private function testCpu(int $uCPU, int $usleep): ?array
    {
        $res           = [];
        $ruStart       = 0;
        $ruEnd         = 0;
        $hashingCycles = 0;
        if ($uCPU > 0) {
            $ruStart = $this->getNowRuUSec();
            do {
                ++$hashingCycles;
                hash('sha256', $this->generateRandomString(70));

                $ruEnd = $this->getNowRuUSec();
            } while ($ruEnd - $ruStart < $uCPU);

            $res['hashing_cycles']      = $hashingCycles;
            $res['cpu_requested_spent'] = $uCPU;
            $res['cpu_extra_spent']     = ($ruEnd - $ruStart) - $uCPU;
        }

        if ($usleep > 0) {
            usleep($usleep);
            $res['usleep'] = $usleep;
        }

        return count($res) > 0 ? $res : null;
    }

    private function getNowRuUSec(): int
    {
        $ruNow = getrusage();
        return $ruNow['ru_utime.tv_sec'] * 1000000 + $ruNow['ru_utime.tv_usec'];
    }

    private function generateRandomString(int $length): string
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
