worker_processes  4;
worker_rlimit_nofile 18000;

events {
    worker_connections 8192;
    multi_accept on;
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    #access_log logs/access.log main;
    access_log off;
    error_log  /var/log/nginx/error.log;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;

    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 70s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }

    server {
        listen 80 default_server;
        return 444;
    }

    server {
        server_name duk.devhands.cloud localhost;
        listen 80 backlog=8192;

        root   /var/www/app/public;
        index  index.php;
        error_page 404 /index.php;

        charset utf-8;

        location / {
            client_max_body_size 20M;

            # Preflighted requests
            if ($request_method = OPTIONS ) {
              add_header "Access-Control-Allow-Origin"  *;
              add_header "Access-Control-Allow-Methods" *;
              add_header "Access-Control-Allow-Headers" *;

              return 200;
            }

            add_header "Access-Control-Allow-Origin"  *;

            try_files $uri $uri/ /index.php?$query_string;
        }

        location /octane {
            try_files $uri $uri/ @octane;
        }

        location ~* \.php$ {
            fastcgi_index   index.php;
            fastcgi_pass    php-service:9000;
            include         fastcgi_params;
            fastcgi_param   SCRIPT_FILENAME    $document_root$fastcgi_script_name;
            fastcgi_param   SCRIPT_NAME        $fastcgi_script_name;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
        }

        location @octane {
            set $suffix "";

            if ($uri = /index.php) {
                set $suffix ?$query_string;
            }

            proxy_http_version 1.1;
            proxy_set_header Host $http_host;
            proxy_set_header Scheme $scheme;
            proxy_set_header SERVER_PORT $server_port;
            proxy_set_header REMOTE_ADDR $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;

            proxy_pass http://php-service:8888$suffix;
        }

        location /fpm-status {
            # Проксируем запросы к pm.status в PHP-FPM
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_param REQUEST_METHOD $request_method;
            fastcgi_param QUERY_STRING $query_string;
            fastcgi_param CONTENT_TYPE $content_type;
            fastcgi_param CONTENT_LENGTH $content_length;
        }

        location /nginx-status {
            stub_status on;
            access_log off;
            #allow 127.0.0.1; # Разрешенный IP для доступа к странице статуса
            #deny all; # Запретить доступ к другим IP
        }
    }
}

